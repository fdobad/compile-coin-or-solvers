Moved to: https://codeberg.org/fdobad/compile-coin-or-solvers

# compile ipopt & SHOT solvers for debian Bookworm

# Get the code
## A. clone

    git clone --recurse-submodules git@gitlab.com:fdobad/compile-coin-or-solvers.git

## B. alternative from scratch

	git init
	git branch -m main
	git submodule init
    git submodule add --name ThirdParty-ASL https://github.com/coin-or-tools/ThirdParty-ASL.git
	git submodule add --name ThirdParty-HSL https://github.com/coin-or-tools/ThirdParty-HSL.git
	git submodule add --name ThirdParty-Mumps https://github.com/coin-or-tools/ThirdParty-Mumps.git
	git submodule add --name Ipopt https://github.com/coin-or/Ipopt.git
	git submodule add --name SHOT https://github.com/coin-or/SHOT.git
    cd SHOT
    git submodule init
    git submodule update
    cd ..

# essentials

	sudo apt install build-essential gfortran libopenmpi-dev

# blas & lapack
## A. free blas & lapack

    sudo apt install libblas-dev liblapack-dev

## B. replace with non-free intel-mkl

	sudo apt install intel-mkl

	Use libmkl_rt.so as the default alternative to BLAS/LAPACK? [yes/no] yes
	Which of the these alternatives should point to MKL? 1 2 3 4

# ASL

    cd ThirdParty-ASL
    ./get.ASL
    ./configure
    make
    sudo make install
    cd ..

# HSL
## Optional: HSL Mathematical Software Library
Get a download link from http://hsl.rl.ac.uk/ipopt, using your academic email (it takes a few days to get the email back)  
Check the "Which solver?" section. Available https://licences.stfc.ac.uk/product/coin-hsl  
Download and unpack to:  

    tar -xf coinhsl-2019.05.21.tar.gz
    mv coinhsl-2019.05.21 ThirdParty-HSL/coinhsl

## Non-optional:

    cd ThirdParty-HSL
    ./configure
    make
    sudo make install
    cd ..

# MUMPS

    cd ThirdParty-Mumps
    ./get.Mumps
    ./configure
    make
    sudo make install
    cd ..

# Ipopt

    mkdir Ipopt/build
    cd Ipopt/build
    ../configure --with-asl=build --with-hsl=build --with-mumps=build
    make
    sudo make install
    cd ..

# SHOT
Requirements: cbc & ipopt

    sudo apt-get install coinor-libcbc-dev coinor-cbc
    mkdir SHOT/build
    cd SHOT/build
    cmake -DCMAKE_BUILD_TYPE=Release -DHAS_AMPL=on -DHAS_CBC=on -DHAS_IPOPT=on ../.
    cmake --build .
    sudo make install
    cd ..

# get pyomo on a venv

    python3 -m venv venv
    sourve venv/bin/activate
    pip install pyomo
    python -m pyomo.contrib.pynumero.build -DBUILD_ASL=ON -DBUILD_MA27=ON -DBUILD_MA57=ON -DIPOPT_DIR=ipopt/build

# Left behind 
## Pardiso
https://www.pardiso-project.org/ -> https://panua.ch/ipopt/  
https://panua.ch/pardiso/#download to generate 12 month academic license  
Check the --with-pardiso option in the next ../configure call below  

    cd Ipopt/build
    ../configure --with-asl=build --with-pardiso="$HOME/source/coin-or/libpardiso600-GNU800-X86-64.so -fopenmp -lgfortran"

## WSMP
The file wsmp.lic must be present in the directory from which you are  
running a program linked with the WSMP libraries.  You can make multiple 
copies of this file for your own personal use.  Alternatively, you can 
place this file in a fixed location and set the environment variable 
WSMPLICPATH to the path of its location.  
[Watson Sparse Matrix Package (WSMP)](https://researcher.watson.ibm.com/researcher/view_group.php?id=1426)  
Check the --with-wsmp option in the next ../configure call below  

    cd Ipopt/build
    ../configure --with-asl=build --with-wsmp="$HOME/source/coin-or/wsmp/wsmp-Linux64-GNU/lib/libwsmp64.a -L$HOME/source/coin-or/wsmp/wsmp-Linux64-GNU/lib64 -lblas -llapack -lpthread -lm -lgfortran"


# Related Links
| About | Link |
| --- | --- |
| Pyomo | https://github.com/Pyomo/pyomo |
| | https://pyomo.readthedocs.io |
| Ipopt | https://coin-or.github.io/Ipopt/INSTALL.html |
| | https://github.com/coin-or/Ipopt |
| | https://coin-or.github.io/Ipopt/OPTIONS.html#OPT_Warm_Start |
| Metis |  http://glaros.dtc.umn.edu/gkhome/metis/metis/download |
| HSL | http://hsl.rl.ac.uk/ipopt |
| WSMP | https://researcher.watson.ibm.com/researcher/view_group_subpage.php?id=7624 |
| SHOT | https://shotsolver.dev/shot/about-shot/compiling |
| | https://github.com/coin-or/SHOT |
| | https://shotsolver.dev/shot/using-shot/solver-options |
| cmake | https://cmake.org/cmake/help/latest/guide/tutorial/index.html |
